import { Document } from 'mongoose';

// Basicamente lo que aca llamo interface es a los modelos, los objetos que no son DTOs. 
// Si no heredo Document no me deja usarlo para el ODM.
export interface Task extends Document{
    id?: number, // El signo significa, puede que tenga o no este field.
    title: string,
    description: string,
    done: boolean,
}