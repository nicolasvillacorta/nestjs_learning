import { Schema } from "mongoose";
import { StringDecoder } from "node:string_decoder";
import { string } from "yargs";

// No uso ID porque lo crea automaticamente.
export const TaskSchema = new Schema({
    title: String,
    description: String,
    done: Boolean
});