import { Controller, Get, Post, Put, Delete, Body, Param, Req, Res } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TasksService } from './tasks.service';
import { Task } from './interfaces/Task';

@Controller('tasks')
export class TasksController {

    // Asi inyecto la dependencia.
    constructor(private taskServices: TasksService){}
    
    @Get()
    getTasks(): Promise<Task[]> {
        return this.taskServices.getTasks();
    }

    @Get(':taskId')
    getTask(@Param('taskId') taskId: string): Promise<Task> {
        return this.taskServices.getTask(taskId);
    }

    @Post()
    createTask(@Body() task: CreateTaskDto): Promise<Task> {
        return this.taskServices.createTask(task);
    }
    
    /*
        Asi lo hice antes de conectar a la DB.

        @Get()
        getTasks(): Task[] {
            return this.taskServices.getTasks();
        }
        // Tambien puedo usar Express que viene integrado con Nest, pero este modo no es recomendable, la idea es que uses
        //      el estilo de Nest. (El de arriba)
        // Habria que importar esto: 'import { Request, Response } from 'express';'

        // getTask(@Req() req,@Res() res) {
        //     console.log(req.body)
        //     res.send("hello world")
        // }

        @Get(':taskId')
        getTask(@Param('taskId') taskId: string): Task {
            // Tengo que hacer parseInt porque el parametro que llega es un string y el id de las tareas es number.
            return this.taskServices.getTask(parseInt(taskId));
        }

        @Post()
        createTask(@Body() task: CreateTaskDto): string {
        return 'Creating a task';
    }

    */
    
    // El @Param es lo mismo que el @PathVariable de Spring Web.
    @Delete(':id')
    deleteTask(@Param('id') id): string {
        console.log(id)
        return `Deleting a task number: ${id}`;
    }

    @Put(':id')
    updateTask(@Body() task: CreateTaskDto, @Param('id') id): string {
        console.log(task);
        console.log(id);
        return 'Updating a task';
    }


}
