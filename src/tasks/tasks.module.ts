import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { TaskSchema } from './schemas/task.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
    imports: [MongooseModule.forFeature([
        {name: 'Task', schema: TaskSchema}
    //  {name: 'Users', schema: UserSchema} -> Aca iria si tuviera mas schemas en este modulo.  
    ])],
    controllers: [TasksController],
    providers: [TasksService]
})
export class TasksModule {}
